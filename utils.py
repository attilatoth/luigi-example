import os
import time


def delay(secs=5):
    time.sleep(secs)


def get_file_name(path):
    return os.path.splitext(path)[0]