import os

import luigi

import analytics
from utils import delay
from utils import get_file_name


# execute:
# python -m luigi --module pipeline WholePipeline --local-scheduler

# luigid --background --port 8888 --pidfile ./example.pid --logdir ./logs --state-path ./state.file


class InputFile(luigi.ExternalTask):
    file = luigi.Parameter()

    def output(self):
        delay()
        dir_name = os.path.dirname(os.path.realpath(__file__))
        return luigi.LocalTarget(os.path.join(dir_name, self.file))


class InputFilter(luigi.Task):
    input_file = luigi.Parameter()
    invalid_names = luigi.Parameter()

    def requires(self):
        return {'data': InputFile(self.input_file), 'invalid_names': InputFile(self.invalid_names)}

    def run(self):
        delay()
        with self.input()['invalid_names'].open('r') as invalid_file:
            invalids = [i.strip() for i in invalid_file.readlines()]

            with self.input()['data'].open('r') as data_file:
                reader = analytics.DataReader(data_file)
                data = reader.read_data()
                valid_filter = analytics.ValidNamesFilter(invalids)
                valid_values = valid_filter.filter(data)

                with self.output().open('w') as out_file:
                    for values in valid_values:
                        line = "{0}\t{1}\n".format(values[0], values[1])
                        out_file.write(line)

    def output(self):
        dir_name = os.path.dirname(os.path.realpath(__file__))
        file_name = get_file_name(self.input_file)
        valid_file = "valid_{0}.tsv".format(file_name)
        output_file = os.path.join(dir_name, valid_file)

        return luigi.LocalTarget(output_file)


class Aggregator(luigi.Task):
    input_file = luigi.Parameter()
    invalid_names = luigi.Parameter()

    def requires(self):
        return InputFilter(self.input_file, self.invalid_names)

    def run(self):
        delay()
        data = []
        with self.input().open('r') as f:
            for line in f:
                name, amount = line.split('\t')
                data.append([name, int(amount)])

        aggregator = analytics.Aggregator(data)
        aggregated_incomes = aggregator.aggregate()

        with self.output().open('w') as f:
            for name, income in aggregated_incomes.items():
                line = "{0}\t{1}\n".format(name, income)
                f.write(line)

    def output(self):
        dir_name = os.path.dirname(os.path.realpath(__file__))
        file_name = get_file_name(self.input_file)
        aggregated_file = "aggregated_{0}.tsv".format(file_name)
        output_file = os.path.join(dir_name, aggregated_file)
        return luigi.LocalTarget(output_file)


class MLModel(luigi.Task):
    input_file = luigi.Parameter()
    invalid_names = luigi.Parameter()

    def requires(self):
        return Aggregator(self.input_file, self.invalid_names)

    def run(self):

        # raise ValueError('achtung, achtung')
        
        delay()
        data = []
        with self.input().open('r') as f:
            for line in f:
                name, amount = line.split('\t')
                data.append([name, int(amount)])
        model = analytics.MLModel(data)
        relative_income_map = model.apply()

        with self.output().open('w') as f:
            for name, weight in relative_income_map.items():
                line = "{0},{1}\n".format(name, weight)
                f.write(line)

    def output(self):
        dir_name = os.path.dirname(os.path.realpath(__file__))
        file_name = get_file_name(self.input_file)
        relative_weights_file = "relative_aggregated_{0}.csv".format(file_name)
        output_file = os.path.join(dir_name, relative_weights_file)
        return luigi.LocalTarget(output_file)


class Reporter(luigi.Task):
    input_file = luigi.Parameter()
    invalid_names = luigi.Parameter()

    def requires(self):
        return MLModel(self.input_file, self.invalid_names)

    def run(self):
        delay()

        data = []
        with self.input().open('r') as input_data:
            for line in input_data:
                values = line.strip().split(',')
                data.append((values[0], float(values[1])))

        reporter = analytics.Reporter(data)
        report_content = reporter.generate_report_data()

        with self.output().open('w') as output_file:
            output_file.write(report_content)

    def output(self):
        dir_name = os.path.dirname(os.path.realpath(__file__))
        file_name = get_file_name(self.input_file)
        report_file_name = "report_{0}.tsv".format(file_name)
        output_file = os.path.join(dir_name, report_file_name)
        return luigi.LocalTarget(output_file)


class WholePipeline(luigi.Task):
    input_file = luigi.Parameter()
    invalid_names = luigi.Parameter()

    def requires(self):
        return Reporter(self.input_file, self.invalid_names)
