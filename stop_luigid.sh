#!/usr/bin/env bash

PIDFILE=luigid.pid

if [ -f "./${PIDFILE}" ]; then
    echo "Stopping luigid..."
    if kill $(cat ${PIDFILE}); then
        sleep 1
        rm -f ${PIDFILE}
    fi
fi

[ $(pgrep luigid 2>/dev/null) ] && echo "Could not stop luigid. It is still running with pid $(pgrep luigid)"

