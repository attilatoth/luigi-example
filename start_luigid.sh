#!/usr/bin/env bash


PIDFILE=luigid.pid

if [ $(pgrep luigid) ]; then
    echo "Luigid is already running."
else
    luigid --background --port 8888 --pidfile ./${PIDFILE} --logdir ./logs --state-path ./luigid.state
    [ $(pgrep luigid) ] || echo "Could not start luigid."
fi
