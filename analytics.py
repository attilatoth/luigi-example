from __future__ import division


class DataReader(object):
    def __init__(self, input_stream):
        self.data_stream = input_stream

    # input file format:
    # name1-amount1
    # name2-amount2
    # name1-amount3
    def read_data(self):
        data = []
        for line in self.data_stream:
            values = line.split('-')
            DataReader.__validate_values(values)
            values = [values[0], int(values[1])]
            data.append(values)
        return data

    @staticmethod
    def __validate_values(values):
        int(values[1])
        if len(values) != 2 or not values[0]:
            raise ValueError("Invalid input file format: {} {}".format(len(values), not values[0]))


class ValidNamesFilter(object):
    '''Filter out the names that match the criteria'''

    def __init__(self, invalids):
        self.invalids = invalids

    def filter(self, data):
        return filter(lambda d: not any(invalid in d[0].lower() for invalid in self.invalids), data)


class Aggregator(object):
    def __init__(self, income_list):
        self.income_list = income_list

    def aggregate(self):
        aggregated_incomes = {}
        for payment in self.income_list:
            name, amount = payment
            aggregated_incomes[name] = aggregated_incomes.get(name, 0) + amount

        return aggregated_incomes


class Reporter(object):
    def __init__(self, input_data):
        self.data = input_data

    def __sort_data_by_ratio(self):
        return sorted(self.data, key=lambda tpl: (-1) * tpl[1])

    def generate_report_data(self):
        sorted_list = self.__sort_data_by_ratio()
        comma_delimited_list = ["{}\t{}".format(tpl[0], tpl[1]) for tpl in sorted_list]
        comma_delimited_list.insert(0, "#name\tratio")

        return "\n".join(comma_delimited_list)


class MLModel(object):
    def __init__(self, data):
        self.data = data

    def apply(self):
        s = sum([d[1] for d in self.data])
        return {entry[0]: entry[1] / s for entry in self.data}
